# deploy-minikube

## Модуль CI пайплайна. Выполняет установку helm чарта в minikube кластер.

Переменные, необходимые для работы:

| Переменная                | Описание                                                          | Значение по умолчанию    |
|---------------------------|-------------------------------------------------------------------|--------------------------|
| `HELM_DEPLOYER_IMAGE`     | Имя образа с установленным `helm`                                 | `"alpine/k8s"`           |
| `HELM_DEPLOYER_IMAGE_TAG` | Тег образа с установленным `helm`                                 | `"1.30.0"`               |
| `MINIKUBE_CD_TOKEN`       | Токен для подключения к кластеру                                  | Нет                      |
| `MINIKUBE_NAMESPACE`      | Неймспейс, в котором будет происходить установка                  | `"example-ns"`           |
| `HELM_DOWNLOAD_USER`      | username токена для скачивания чарта из `Gitlab package registry` | Нет                      |
| `HELM_DOWNLOAD_SECRET`    | secret токена для скачивания чарта из `Gitlab package registry`   | Нет                      |
| `HELM_CHART_NAME`         | Имя helm чарта                                                    | `"example-chart"`        |
| `HELM_CHART_RELEASE`      | Релиз helm чарта                                                  | `"example-release"`      |
| `DOCKER_IMAGE_REGISTRY`   | Имя docker образа приложения                                      | `${CI_REGISTRY_IMAGE}`   |
| `DOCKER_IMAGE_TAG`        | Тег docker образа приложения                                      | `${CI_COMMIT_SHORT_SHA}` |
| `HELM_CHART_VERSION`      | Версия helm чарта                                                 | `"0.0.1"`                |
| `APP_URL`                 | URL приложения                                                    | `"example.avp.com"`      |

Для использования модуля в пайплайне необходимо вставить следующий код:

```
stages:
  - stage

...

include:
  - project: cicd-includes/deploy-minikube
    file: deploy-minikube.yml
    ref: main

...

deploy-minikube-work:
  stage: stage
  extends:
    - .deploy-stage
  variables:
    MINIKUBE_CD_TOKEN: <YOUR TOKEN TO ACCESS TO MINIKUBE CLUSTER>
```